# Mailman Suite

This is a skeleton django project that can be used to serve postorius and/or hyperkitty.

## Compatibilty
The master branch will most likely (but is not guaranteed) be in sync with the master branches of hyperkitty and postorius.  

| Mailman Suite | Hyperkitty | Postorius | Django |
| :-----------: | :--------: | :-------: | :----: |
| `master`-branch | commit 5b332df8 | commit 711f6339 | v1.10.5 |

## Installation

Installation really depends on what you want to do. Here are my recommendation for development and production. You don't have to follow them, but they might serve as a starting point.

If you use this document to setup a production server:  
Don't take anything for granted! Understand what each command /configuration option does before you use it! Follow at your own risk :-)


### Gitlab setup (needed for development)
* Register on gitlab.com
* Generate a ssh-key using `ssh-keygen` and upload the public key to your gitlab account
* Fork the projects you want to contribute to (mailman, postorius, hyperkity, mailmanclient, django-mailman3)
* Clone your forks to some directory (`~/dev/mailman` for instance)
* For every fork add an upstream url using `git remote add upstream gitlab@gitlab.com/maliman/<repo_name>` so you can download updates


### Development setup
```
NOTE: The actual commands depend on your OS
```
* Change to your dev directory `cd ~/dev/mailman`
* Create a python3 virtualenv `virtualenv3 env`
* Create a python2 virtualenv `virtualenv2 env2`  
* Enter the python3 env `source env/bin/activate`
* Install mailman `cd mailman; python setup.py develop; cd ..`
* Exit the env `deactivate` and enter the python2 one `source env2/bin/activate`
* Install mailmanclient `cd mailmanclient; python setup.py develop; cd ..`
* Install django-mailman3 `cd django-mailman3; python setup.py develop; cd ..`
* Install postorius `cd postorius; python setup.py develop; cd ..`
* Install hyperkitty `cd hyperkitty; python setup.py develop; cd ..`
* To start development, uncomment the project you want to develop in the installed_apps section
* Read the Database section on how to populate the database
* You might want to read the configuration section to know what else can be edited. It is also
  recommened to read the settings file.

#### Development-Server
Be sure to activate your python2 virtualenv first!  
In case you want to use this for development or testing only, you can start the server using `python manage.py runserver`. Using this server is not intended for
production use as it severely impacts overall security.

### Production setup
* The recommended place to install mailman to is `/var/lib/mailman`
* You will have to choose if you want to install all the python packages systemwide using your packagemanager and/or pip or use a virtualenv.
* Create a user for mailman `useradd -m -b /var/lib -s /bin/false mailman`
* If you want to have a virtualenv go ahead and create a python3 virtualenv inside mailman's home directory `/var/lib/mailman`
* Install the latest released mailman package using `pip install mailman`
* Hyperkitty needs an archiver plugin which should also be installed in the python3 virtualenv using `pip install mailman-hyperkitty`
* You should move this repo (`mailman-suite`) someplace where you want to serve it from. I chose `/srv/django/mailman`.
* If you choose virtualenvs go ahead and create one in `/srv/django/mailman`
* In this virtualenv you should install postorius and/or hyperkitty `pip install postorius hyperkitty`
* Follow the instructions in the configuration section to complete the installation

## Configuration

There are a number of things you need to edit before you can start deploying. Be sure to read the whole document and make sure to read all comments in `nginx.conf`, `mailman.ini` and `mailman_suite/settings.py`

in `mailman_suite/settings.py` you will want to edit the following:  
* Replace the `SECRET_KEY` with a random string
* Add your FQDN to `ALLOWED_HOSTS`
* Uncomment `postorius` and/or `hyperkitty` from `INSTALLED_APPS` depending on what you want to serve
* Currently both hyperkitty and postorius only support english. In the future changing `LANGUAGE_CODE` will change the default display language
* Static and media files are placed inside a `public` folder by default. You will have to create that folder. If you want to change the location, edit `STATIC_ROOT` accordingly.
* Adjust the Mailman API settings namely `MAILMAN_REST_API_URL`, `MAILMAN_REST_API_USER` and `MAILMAN_REST_API_PASS` to whatever Mailman is configured to
* Add emails to `ADMINS` that are notified in production mode in case any exceptions are thrown.
* Be sure to activate the provided `crontab` file
* __Do not deploy this project in production mode unless `DEBUG` is set to `False`!__

## Hyperkitty specific configuration
In theory you can use different backends for `haystack` which is the search backend being used. The default is `whoosh`. To use that be sure to install the python module.

You will have to edit `MAILMAN_ARCHIVER_KEY` and `MAILMAN_ARCHIVER_FROM` to reflect the configuration of the `mailman_hyperkitty` plugin.
Be sure to use the actual public ip address of your mailman installation. It will __not__ work with the provided configuration!

## Database
You can use django with SQlite, postgresql or mysql databases.
Uncomment and edit the block for whatever database you want to use.  
__By default sqlite is being used. You might want to change it for production!__
* __Sqlite__:  
You can use SQlite for testing purposes and for development. However, it is not recommended for use in production environments.
* __Postgresql__:  
In case you opt for Postgresql make sure you install `psycopg2`.
* __Mysql__:  
In case you opt for Mysql make sure you install `mysqlclient`.

After setting up the database run:
`python manage.py migrate` to populate the database.

You will need to create a superuser by using: `python manage.py createsuperuser`

## Fine Tuning
In case you want to fine tune your application you can edit the options below the `ADMINS` entry. You will probably not need to change any of these settings. Some of these settings are critical for postorius and hyperkitty to function!

In case you want to change the default page, edit `mailman_suite\urls.py`.

## Deployment
Be sure to collect all the static files using `python manage.py collectstatic`  
To populate the locales run `python manage.py compilemessages`


## Starting mailman
* load your virtualenv you if installed mailman in one
* change to the directory where you want mailman to store it's data in. In case of production cd into mailman's home `/var/lib/mailman`
* Start mailman using `mailman start`
* mailman will create a bunch of directories in `var` in the current directory.
* `./var/etc/mailman.cfg` contains mailman's settings. Edit it to your liking.
* In case you want mailman to forward mail to hyperkitty for archiving you will have to add the
  necessary configuration.


## Starting postorius and or hyperkitty
### Using uwsgi and nginx

`mailman.ini` contains the options for uwsgi.
You might need to modify the options:
* `chdir` should point to the root of the django project
* In case you use a virtualenv for deployment be sure to specify the correct path to it using `virtualenv`
* You can use `processes` to specify how many workers should be spawned by uwsgi
* Be sure that the user that is going to run uwsgi has write access to the directory listed in `socket`
* In case you run into permission errors you can set the socket permissions using `chmod-socket` However, this should not be necessary in case nginx and uwsgi are run by the same user
* set `uid` and `gid` to the user and group that should run uwsgi

`nginx.conf` contains nginx server sections that can be used. It is prepopulated with options that are recommended by the author.
Be sure to read up on all the options being used. You will have to modify some options:
* set the `server` in the `upstream` definition to the path specified in `mailman.ini`
* modify both `server_name` options to your FQDN
* set the correct path to your ssl certificate and key in `ssl_certificate` and `ssl_certificate_key`
* set the correct path for the `/static` block

Finally create a simlink from `/etc/uwsgi/mailman.ini` to `mailman.ini` and one from `/etc/nginx/sites-enabled/mailman.conf` to `nginx.conf`  
Also make sure that you are actually importing all the `*.conf` files in `/etc/nginx/sites-enabled`  
Reload nginx and start uwsgi using the mailman config.
The latter should be possible with `systemctl start uwsgi@mailman`

## Contributing
Please feel free to submit issues and or merge requests with suggestions to improve the documentation or the default settings.
Providing other skeletons for other deployment options is also welcome
