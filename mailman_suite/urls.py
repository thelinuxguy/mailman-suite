"""mailman_suite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'accounts/', include('allauth.urls')),
    url('', include('django_mailman3.urls')),
]
if 'hyperkitty' in settings.INSTALLED_APPS:
    from hyperkitty.views.index import index as hyperkitty_index
    urlpatterns += [
            url(r'^$', hyperkitty_index),
            url(r'^hyperkitty/', include('hyperkitty.urls')),
            ]
if 'postorius' in settings.INSTALLED_APPS:
    from postorius.views.list import list_index as postorius_index
    urlpatterns += [ 
            url(r'^$', postorius_index),
            url(r'^postorius/', include('postorius.urls')),
            ]

