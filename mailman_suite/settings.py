"""
Django settings for mailman_suite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.contrib.messages import constants as messages

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# TODO SECURITY WARNING: change the secret and keep it secret!
SECRET_KEY = 'by5-x5z)a43vayro*s$3zo9tc!(7cgy^jkqu)r*qpcwc(y7-ud'

# TODO SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# TODO: Add your own hostnames for production mode. Django wont serve them without this!
ALLOWED_HOSTS = ['lists.example.com',]

SITE_ID = 1


# Application definition
# TODO: uncomment hyperkitty and or postorius to enable them
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    #'postorius',
    #'hyperkitty',
    'django_mailman3',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'django_gravatar',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django_mailman3.middleware.TimezoneMiddleware',
)

ROOT_URLCONF = 'mailman_suite.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django_mailman3.context_processors.common',
            ],
        },
    },
]

WSGI_APPLICATION = 'mailman_suite.wsgi.application'

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        )

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'allauth.account.auth_backends.AuthenticationBackend',
        )

# Allauth
ACCOUNT_AUTHENTICATION_METHOD = 'username_email'
ACCOUNT_EMAIL_REAUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'manndatory'
ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'
ACCOUNT_UNIQUE_EMAIL = True

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
# TODO Change the parameters so they match your database setup!
# TODO To use mysql or postgresql uncomment and rename it to 'default'.
#      In that case comment the sqlite block
# NOTE mysqlclient is needed to enable mysql support
# NOTE psycopg2 is needed to enable postgresql support
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    #'mysql': {
    #    'ENGINE': 'django.db.backends.mysql',
    #    'NAME': 'mailman-django-db',
    #    'USER': 'mailman-django',
    #    'PASSWORD': 'very_secret_pass',
    #},
    #'psql': {
    #    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #    'NAME': 'mailman-django-db',
    #    'USER': 'mailman-django',
    #    'PASSWORD': 'very_secret_pass',
    #},
}

STATIC_URL = '/static/'

# TODO create the public folder
STATIC_ROOT = os.path.join(BASE_DIR, 'public/static')

# Mailman API credentials
# TODO adjust accordingly
MAILMAN_REST_API_URL = 'http://localhost:8001'
MAILMAN_REST_API_USER = 'restadmin'
MAILMAN_REST_API_PASS = 'restpass'

# TODO change to reflect mailman_hyperkitty's settings
# You only have to edit them if you actually use hyperkitty
# You have to specify the actual public ip addresses,
# the ones provided below will NOT work
MAILMAN_ARCHIVER_KEY = 'secretapikey'
MAILMAN_ARCHIVER_FROM = ('127.0.0.1', '::1')

# Admin emails that get notified of error during production
# TODO add your emails here
ADMINS = (
        ('admin', 'root@localhost'),
        )


###############################################################################
## YOU PROBABLY DO NOT WANT TO CHANGE ANYTHING BELOW THIS LINE               ##
## MODIFY AT YOUR OWN RISK                                                   ##
###############################################################################

LOGIN_URL = 'account_login'
LOGOUT_URL = 'account_logout'

if 'postorius' in INSTALLED_APPS:
    TEMPLATES[0]['OPTIONS']['context_processors'] += ('postorius.context_processors.postorius',)
    LOGIN_REDIRECT_URL = '/postorius/'
    MESSAGE_TAGS = { messages.ERROR: 'danger' }
if 'hyperkitty' in INSTALLED_APPS:
    LOGIN_REDIRECT_URL = '/hyperkitty/'
    INSTALLED_APPS += (
            'rest_framework',
            'paintstore',
            'compressor',
            'haystack',
            'django_extensions',
            )
    STATICFILES_FINDERS += (
            'compressor.finders.CompressorFinder',
            )
    cp = (
            'hyperkitty.context_processors.export_settings',
            )
    TEMPLATES[0]['OPTIONS']['context_processors'] += cp
    SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
    ANONYMOUS_USER_ID = -1

    COMPRESS_PRECOMPILERS = (
      ('text/x-scss', 'sassc {infile} {outfile}'),
      ('text/x-sass', 'sassc {infile} {outfile}'),
      )
    #
    # Full-text search engine
    #
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(BASE_DIR, "fulltext_index"),
        },
    }

    # This is used for branding
    APP_NAME = 'Mailman'

    # Allow authentication with the internal user database
    # If disabled Persona or some other OAUTH provides has to be used
    USE_INTERNAL_AUTH = True

    # Only display mailing-lists from the same virtual host as the webserver
    FILTER_VHOST = False

    # This is for development purposes
    USE_MOCKUPS = False
